package com.jbr320.splitstringapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SplitStringAPI {
    @CrossOrigin
    @GetMapping("/split")
    public ArrayList<String> splitString(@RequestParam(required = true) String str) {
        ArrayList<String> stringList = new ArrayList<String>();
        String[] wordList = str.split(" "); 
        for (String word : wordList) {
            stringList.add(word);
        }
        return stringList;
    }
}